﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Airport.Entities
{
    [DataContract]
    public class Vol
    {
        [DataMember]
        public int IdVol { get; set; }
        [DataMember]
        public string Compagnie { get; set; }
        [DataMember]
        public string Ligne { get; set; }
        [DataMember]
        public int JourExploitation { get; set; }
        [DataMember]
        public StatutVol Statut { get; set; }
        [DataMember]
        public string TypeAvion { get; set; }
        [DataMember]
        public string Immatriculation { get; set; }
        [DataMember]
        public string Parking { get; set; }
        [DataMember]
        public DateTime DernierHoraire { get; set; }
        [DataMember]
        public Char OrigineCreation { get; set; }
        //[DataMember]
        //public List<string> Bagages { get; set; }
        [DataMember]
        public List<string> Itineraire { get; set; }
        [DataMember]
        public List<string> Banques { get; set; }
    }
}
