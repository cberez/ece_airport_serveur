﻿using System;
using System.Runtime.Serialization;

namespace Airport.Entities
{
    // Table TRACE_BAGAGE
    [DataContract]
    public class Trace
    {
        [DataMember]
        public int IdTrace { get; set; }

        [DataMember]
        public int IdBagage { get; set; }

        [DataMember]
        public DateTime Horodate { get; set; }

        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public int Localisation { get; set; }

        [DataMember]
        public string Information { get; set; }

        [DataMember]
        public int Statut { get; set; }
    }
}
