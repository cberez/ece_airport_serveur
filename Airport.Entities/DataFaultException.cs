﻿using System.Runtime.Serialization;

namespace Airport.Entities
{
    [DataContract]
    public class DataFaultException
    {
        [DataMember] 
        public string Message;

        [DataMember] 
        public string ColumnName;
    }
}
