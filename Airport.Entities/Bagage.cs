﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Airport.Entities
{
    // Table BSM
    [DataContract]
    public class Bagage
    {
        [DataMember]
        public int IdBagage { get; set; }

        [DataMember]
        public String CodeIata { get; set; }

        [DataMember]
        public DateTime DateCreation { get; set; }

        [DataMember]
        public DateTime DateModification { get; set; }

        [DataMember]
        public Vol Vol { get; set; }

        [DataMember]
        public List<Trace> Traces { get; set; }
    }
}
