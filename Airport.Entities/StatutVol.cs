﻿namespace Airport.Entities
{
    public enum StatutVol
    {
        Annulé,
        Parti,
        Normal,
        Supprimé,
        Parking,
        Approche,
        MiseEnPlace,
        Chargement
    }
}
