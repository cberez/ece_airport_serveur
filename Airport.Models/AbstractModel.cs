﻿using Airport.Entities;
using System;
using System.Collections.Generic;

namespace Airport.Models
{
    abstract public class AbstractModel
    {
        #region Vols

        #region CRUD
        public abstract void CreateVol(string cie, string line, int jourEx, string statut, string immatriculation, string pkg, DateTime dernierHoraire, char origineCreation, string typeAvion);
        public abstract void UpdateVol(int idVol, string cie, string line, int jourEx, string statut, string immatriculation, string pkg, DateTime dernierHoraire, char origineCreation, string typeAvion);
        public abstract void DeleteVol(int idVol);
        #endregion

        #region GET
        public abstract Vol GetDetailsVol(int idVol);
        public abstract List<Vol> GetVols();
        public abstract List<Vol> GetVols(DateTime debut, DateTime fin);
        public abstract List<Vol> GetVols(string cie, string line, DateTime debut, DateTime fin);  
        #endregion

        #endregion


        #region Bagages

        #region CRUD
        abstract public void CreateBagage(string codeIata, DateTime dateCreation, int idVol, string cie, string ligne);
        abstract public void UpdateVolBagage(int idBagage, int idVol);
        abstract public void DeleteBagageByCodeAndDate(string codeIata, DateTime dateCreation);
        abstract public void DeleteBagageById(int id);
        abstract public void DeleteBagageByIdVol(int idVol);
        abstract public void AddTraceBagage(int idBagage, DateTime horodate, int type, int localisation, string information, int statut);
        #endregion

        #region GET
        abstract public Bagage GetDetailBagage(int idBagage);
        abstract public List<Bagage> GetBagagesByCodeIata(string codeIata, DateTime debut, DateTime fin);
        public abstract List<Bagage> GetBagages(string cie, string line, DateTime debut, DateTime fin);
        abstract public List<Bagage> GetBagagesByIdVol(int idVol);
        //abstract public List<Bagage> FindBagageByLocalisation(DateTime debut, DateTime fin, List<int> localisations);
        //abstract public List<Bagage> FindBagageByTypeTrace(DateTime debut, DateTime fin, List<int> typeTraces);
        abstract public List<Trace> GetBagageTrace(int idBagage);

        #endregion
        #endregion

    }
}
