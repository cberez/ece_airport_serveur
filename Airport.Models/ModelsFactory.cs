﻿using Airport.Models;

namespace Airport
{
    public static class ModelsFactory
    {
        private static AbstractModel _singleton;
        private static string model = "Sql";

        public static AbstractModel Model
        {
            get
            {
                if (_singleton == null)
                {
                    switch (model)
                    {
                        case "Sql":
                            _singleton = new ModelSql();
                            break;
                        case "Natif":
                            //singleton = new Models.ModelNatif();
                            break;
                        default:
                            //singleton = new Models.ModelNatif();
                            break;
                    }

                }
                return _singleton;
            }
        }
    }
}
