﻿using Airport.Models;

namespace Airport
{
    public class AbstractModelFactory
    {
        private AbstractModelFactory(){}

        static public AbstractModel AbstractModel(string modelType)
        {
            switch (modelType)
            {
                case "Sql":
                    return new ModelSql();
                default:
                    return new ModelSql();
            }
        }
    }
}
