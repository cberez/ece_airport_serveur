﻿using Airport.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Airport.Models
{
    public class ModelSql : AbstractModel
    {
        private const string ConnectionString = @"Data Source=.\sqlexpress;Initial Catalog=CDG1_EXPLOIT_ADP;Integrated Security=True";

        // TODO idVol, cie, line, jex can't be null 
        public override void CreateVol(string cie, string line, int jourEx, string statut, string immatriculation, string pkg, DateTime dernierHoraire, char origineCreation, string typeAvion)
        {
            using (var cnx = new SqlConnection(ConnectionString))
            {
                var cmd = new SqlCommand("insertVol", cnx)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.Add("@compagnie", SqlDbType.VarChar);
                cmd.Parameters.Add("@ligne", SqlDbType.VarChar);
                cmd.Parameters.Add("@joursExploit", SqlDbType.Int);
                cmd.Parameters.Add("@dernierHoraire", SqlDbType.DateTime);
                cmd.Parameters.Add("@statut", SqlDbType.VarChar);
                cmd.Parameters.Add("@parking", SqlDbType.VarChar);
                cmd.Parameters.Add("@typeAvion", SqlDbType.VarChar);
                cmd.Parameters.Add("@immatriculation", SqlDbType.VarChar);
                cmd.Parameters.Add("@origineCreation", SqlDbType.VarChar);
                cmd.Parameters[0].Value = cie;
                cmd.Parameters[1].Value = line;
                cmd.Parameters[2].Value = jourEx;
                cmd.Parameters[3].Value = dernierHoraire;
                cmd.Parameters[4].Value = statut;
                cmd.Parameters[5].Value = pkg;
                cmd.Parameters[6].Value = typeAvion;
                cmd.Parameters[7].Value = immatriculation;
                cmd.Parameters[8].Value = origineCreation;
                cnx.Open();

                SqlDataReader sdr = cmd.ExecuteReader();
            }
        }

        public override void UpdateVol(int idVol, string cie, string line, int jourEx, string statut, string immatriculation, string pkg, DateTime dernierHoraire, char origineCreation, string typeAvion)
        {
            using (var cnx = new SqlConnection(ConnectionString))
            {
                var cmd = new SqlCommand("insertVol", cnx)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.Add("@compagnie", SqlDbType.VarChar);
                cmd.Parameters.Add("@ligne", SqlDbType.VarChar);
                cmd.Parameters.Add("@joursExploit", SqlDbType.Int);
                cmd.Parameters.Add("@dernierHoraire", SqlDbType.DateTime);
                cmd.Parameters.Add("@statut", SqlDbType.VarChar);
                cmd.Parameters.Add("@parking", SqlDbType.VarChar);
                cmd.Parameters.Add("@typeAvion", SqlDbType.VarChar);
                cmd.Parameters.Add("@immatriculation", SqlDbType.VarChar);
                cmd.Parameters.Add("@origineCreation", SqlDbType.VarChar);
                cmd.Parameters.Add("@idVol", SqlDbType.Int);
                cmd.Parameters[0].Value = cie;
                cmd.Parameters[1].Value = line;
                cmd.Parameters[2].Value = jourEx;
                cmd.Parameters[3].Value = dernierHoraire;
                cmd.Parameters[4].Value = statut;
                cmd.Parameters[5].Value = pkg;
                cmd.Parameters[6].Value = typeAvion;
                cmd.Parameters[7].Value = immatriculation;
                cmd.Parameters[8].Value = origineCreation;
                cmd.Parameters[9].Value = idVol;
                cnx.Open();

                SqlDataReader sdr = cmd.ExecuteReader();
            }
        }

        public override void DeleteVol(int idVol)
        {
            using (var cnx = new SqlConnection(ConnectionString))
            {
                var cmd = new SqlCommand("deleteVol", cnx)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.Add("@idVol", SqlDbType.Int);
                cmd.Parameters[0].Value = idVol;
                cnx.Open();

                SqlDataReader sdr = cmd.ExecuteReader();

            }
        }

        public override Vol GetDetailsVol(int idVol)
        {
            var vol = new Vol();
            var banques = new List<string>();
            var itineraire = new List<string>();

            using (var cnx = new SqlConnection(ConnectionString))
            {
                var cmd = new SqlCommand("selectDetailsVol", cnx)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.Add("@idVol", SqlDbType.Int);
                cmd.Parameters[0].Value = idVol;
                cnx.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        vol.IdVol = sdr.GetInt32(sdr.GetOrdinal("ID_VOL"));
                        vol.Compagnie = sdr.GetString(sdr.GetOrdinal("Compagnie"));
                        vol.Ligne = sdr.GetString(sdr.GetOrdinal("Ligne"));
                        vol.JourExploitation = sdr.GetInt16(sdr.GetOrdinal("JourExploitation"));
                        vol.TypeAvion = sdr["TypeAvion"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("TypeAvion"));
                        vol.Immatriculation = sdr["Immatriculation"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("Immatriculation"));
                        vol.Parking = sdr["Parking"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("Parking"));
                        vol.DernierHoraire = sdr["DernierHoraire"] is DBNull ? DateTime.MaxValue : sdr.GetDateTime(sdr.GetOrdinal("DernierHoraire"));
                        vol.OrigineCreation = sdr["OrigineCreation"] is DBNull ? '_' : sdr.GetString(sdr.GetOrdinal("OrigineCreation")).ToCharArray()[0];
                        switch (sdr.GetString(sdr.GetOrdinal("Statut")))
                        {
                            case "ANN":
                                vol.Statut = StatutVol.Annulé;
                                break;
                            case "PAR":
                                vol.Statut = StatutVol.Parti;
                                break;
                            case "NORM":
                                vol.Statut = StatutVol.Normal;
                                break;
                            case "SUP":
                                vol.Statut = StatutVol.Supprimé;
                                break;
                            default:
                                vol.Statut = StatutVol.Normal;
                                break;
                        }
                    }

                    sdr.NextResult();

                    while (sdr.Read())
                    {
                        var v = sdr["Ville"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("Ville"));
                        itineraire.Add(v);
                    }
                    vol.Itineraire = itineraire;

                    sdr.NextResult();

                    while (sdr.Read())
                    {
                        var b = sdr["Banque"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("Banque"));
                        banques.Add(b);
                    }
                    vol.Banques = banques;
                }

            }
            return vol;
        }

        public override List<Vol> GetVols()
        {
            var res = new List<Vol>();
            using (var cnx = new SqlConnection(ConnectionString))
            {
                var cmd = new SqlCommand("selectAllVols", cnx)
                {
                    CommandType = CommandType.StoredProcedure
                };
                cnx.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {

                        var vol = new Vol
                        {
                            IdVol = sdr.GetInt32(sdr.GetOrdinal("ID_VOL")),
                            Compagnie = sdr.GetString(sdr.GetOrdinal("Compagnie")),
                            Ligne = sdr.GetString(sdr.GetOrdinal("Ligne")),
                            JourExploitation = sdr.GetInt16(sdr.GetOrdinal("JourExploitation")),
                            TypeAvion = sdr["TypeAvion"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("TypeAvion")),
                            Immatriculation = sdr["Immatriculation"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("Immatriculation")),
                            Parking = sdr["Parking"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("Parking")),
                            DernierHoraire = sdr["DernierHoraire"] is DBNull ? DateTime.MaxValue : sdr.GetDateTime(sdr.GetOrdinal("DernierHoraire")),
                            OrigineCreation = sdr["OrigineCreation"] is DBNull ? '_' : sdr.GetString(sdr.GetOrdinal("OrigineCreation")).ToCharArray()[0]
                        };
                        switch (sdr.GetString(sdr.GetOrdinal("Statut")))
                        {
                            case "ANN":
                                vol.Statut = StatutVol.Annulé;
                                break;
                            case "PAR":
                                vol.Statut = StatutVol.Parti;
                                break;
                            case "NORM":
                                vol.Statut = StatutVol.Normal;
                                break;
                            case "SUP":
                                vol.Statut = StatutVol.Supprimé;
                                break;
                            default:
                                vol.Statut = StatutVol.Normal;
                                break;
                        }

                        res.Add(vol);
                    }
                }
            }
            return res;
        }

        public override List<Vol> GetVols(DateTime debut, DateTime fin)
        {
            var res = new List<Vol>();
            using (var cnx = new SqlConnection(ConnectionString))
            {
                var cmd = new SqlCommand("selectAllVolsDates", cnx)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.Add("@dateDebut", SqlDbType.DateTime);
                cmd.Parameters.Add("@dateFin", SqlDbType.DateTime);
                cmd.Parameters[0].Value = debut;
                cmd.Parameters[1].Value = fin;
                cnx.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        var vol = new Vol
                        {
                            IdVol = sdr.GetInt32(sdr.GetOrdinal("idVol")),
                            Compagnie = sdr.GetString(sdr.GetOrdinal("Compagnie")),
                            Ligne = sdr.GetString(sdr.GetOrdinal("Ligne")),
                            JourExploitation = sdr.GetInt16(sdr.GetOrdinal("JourExploitation")),
                            TypeAvion = sdr["TypeAvion"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("TypeAvion")),
                            Immatriculation = sdr["Immatriculation"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("Immatriculation")),
                            Parking = sdr["Parking"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("Parking")),
                            DernierHoraire = sdr["DernierHoraire"] is DBNull ? DateTime.MaxValue : sdr.GetDateTime(sdr.GetOrdinal("DernierHoraire")),
                            OrigineCreation = sdr["OrigineCreation"] is DBNull ? '_' : sdr.GetString(sdr.GetOrdinal("OrigineCreation")).ToCharArray()[0]
                        };
                        switch (sdr.GetString(sdr.GetOrdinal("Statut")))
                        {
                            case "ANN":
                                vol.Statut = StatutVol.Annulé;
                                break;
                            case "PAR":
                                vol.Statut = StatutVol.Parti;
                                break;
                            case "NORM":
                                vol.Statut = StatutVol.Normal;
                                break;
                            case "SUP":
                                vol.Statut = StatutVol.Supprimé;
                                break;
                            default:
                                vol.Statut = StatutVol.Normal;
                                break;
                        }
                    }
                }
            }
            return res;
        }

        public override List<Vol> GetVols(string cie, string line, DateTime debut, DateTime fin)
        {
            var res = new List<Vol>();
            using (var cnx = new SqlConnection(ConnectionString))
            {
                var cmd = new SqlCommand("selectAllVolsDatesCieLine", cnx)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.Add("@compagnie", SqlDbType.VarChar);
                cmd.Parameters.Add("@line", SqlDbType.VarChar);
                cmd.Parameters.Add("@dateDebut", SqlDbType.DateTime);
                cmd.Parameters.Add("@dateFin", SqlDbType.DateTime);
                cmd.Parameters[0].Value = cie;
                cmd.Parameters[1].Value = line;
                cmd.Parameters[2].Value = debut;
                cmd.Parameters[3].Value = fin;
                cnx.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        var vol = new Vol
                        {
                            IdVol = sdr.GetInt32(sdr.GetOrdinal("idVol")),
                            Compagnie = sdr.GetString(sdr.GetOrdinal("Compagnie")),
                            Ligne = sdr.GetString(sdr.GetOrdinal("Ligne")),
                            JourExploitation = sdr.GetInt16(sdr.GetOrdinal("JourExploitation")),
                            TypeAvion = sdr["TypeAvion"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("TypeAvion")),
                            Immatriculation = sdr["Immatriculation"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("Immatriculation")),
                            Parking = sdr["Parking"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("Parking")),
                            DernierHoraire = sdr["DernierHoraire"] is DBNull ? DateTime.MaxValue : sdr.GetDateTime(sdr.GetOrdinal("DernierHoraire")),
                            OrigineCreation = sdr["OrigineCreation"] is DBNull ? '_' : sdr.GetString(sdr.GetOrdinal("OrigineCreation")).ToCharArray()[0]
                        };
                        switch (sdr.GetString(sdr.GetOrdinal("Statut")))
                        {
                            case "ANN":
                                vol.Statut = StatutVol.Annulé;
                                break;
                            case "PAR":
                                vol.Statut = StatutVol.Parti;
                                break;
                            case "NORM":
                                vol.Statut = StatutVol.Normal;
                                break;
                            case "SUP":
                                vol.Statut = StatutVol.Supprimé;
                                break;
                            default:
                                vol.Statut = StatutVol.Normal;
                                break;
                        }
                    }
                }
            }
            return res;
        }

        // TODO codeIata & DateCrea can't be null
        public override void CreateBagage(string codeIata, DateTime dateCreation, int idVol, string cie, string ligne)
        {
            using (var cnx = new SqlConnection(ConnectionString))
            {
                var cmd = new SqlCommand("insertBagage", cnx)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.Add("@codeIata", SqlDbType.VarChar);
                cmd.Parameters.Add("@dateCreation", SqlDbType.DateTime);
                cmd.Parameters.Add("@idVol", SqlDbType.Int);
                cmd.Parameters.Add("@cie", SqlDbType.VarChar);
                cmd.Parameters.Add("@ligne", SqlDbType.VarChar);
                cmd.Parameters[0].Value = codeIata;
                cmd.Parameters[1].Value = dateCreation;
                cmd.Parameters[2].Value = idVol;
                cmd.Parameters[3].Value = cie;
                cmd.Parameters[4].Value = ligne;
                cnx.Open();

                SqlDataReader sdr = cmd.ExecuteReader();
            }
        }

        public override void UpdateVolBagage(int idBagage, int idVol)
        {
            using (var cnx = new SqlConnection(ConnectionString))
            {
                var cmd = new SqlCommand("updateVolBagage", cnx)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.Add("@id", SqlDbType.Int);
                cmd.Parameters.Add("@idVol", SqlDbType.VarChar);
                cmd.Parameters[0].Value = idBagage;
                cmd.Parameters[1].Value = idVol;
                cnx.Open();

                SqlDataReader sdr = cmd.ExecuteReader();
            }
        }

        public override void DeleteBagageByCodeAndDate(string codeIata, DateTime dateCreation)
        {
            using (var cnx = new SqlConnection(ConnectionString))
            {
                var cmd = new SqlCommand("deleteBagageByCodeAndDate", cnx)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.Add("@iataCode", SqlDbType.VarChar);
                cmd.Parameters.Add("@dateCrea", SqlDbType.DateTime);
                cmd.Parameters[0].Value = codeIata;
                cmd.Parameters[1].Value = dateCreation;
                cnx.Open();

                SqlDataReader sdr = cmd.ExecuteReader();

            }
        }

        public override void DeleteBagageById(int id)
        {
            using (var cnx = new SqlConnection(ConnectionString))
            {
                var cmd = new SqlCommand("deleteBagageById", cnx)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.Add("@id", SqlDbType.Int);
                cmd.Parameters[0].Value = id;
                cnx.Open();

                SqlDataReader sdr = cmd.ExecuteReader();
            }
        }

        public override void DeleteBagageByIdVol(int idVol)
        {
            using (var cnx = new SqlConnection(ConnectionString))
            {
                var cmd = new SqlCommand("deleteBagageById", cnx)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.Add("@idVol", SqlDbType.Int);
                cmd.Parameters[0].Value = idVol;
                cnx.Open();

                SqlDataReader sdr = cmd.ExecuteReader();
            }
        }

        public override void AddTraceBagage(int idBagage, DateTime horodate, int type, int localisation, string information, int statut)
        {
            using (var cnx = new SqlConnection(ConnectionString))
            {
                var cmd = new SqlCommand("insertTrace", cnx)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.Add("@idBsm", SqlDbType.VarChar);
                cmd.Parameters.Add("@horodate", SqlDbType.DateTime);
                cmd.Parameters.Add("@type", SqlDbType.Int);
                cmd.Parameters.Add("@localisation", SqlDbType.VarChar);
                cmd.Parameters.Add("@information", SqlDbType.VarChar);
                cmd.Parameters.Add("@statut", SqlDbType.VarChar);
                cmd.Parameters[0].Value = idBagage;
                cmd.Parameters[1].Value = horodate;
                cmd.Parameters[2].Value = type;
                cmd.Parameters[3].Value = localisation;
                cmd.Parameters[4].Value = information;
                cmd.Parameters[5].Value = statut;
                cnx.Open();

                SqlDataReader sdr = cmd.ExecuteReader();
            }
        }

        public override Bagage GetDetailBagage(int idBagage)
        {
            var bagage = new Bagage();
            var traces = new List<Trace>();

            using (var cnx = new SqlConnection(ConnectionString))
            {
                var cmd = new SqlCommand("selectDetailsBagage", cnx)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.Add("@idBagage", SqlDbType.Int);
                cmd.Parameters[0].Value = idBagage;
                cnx.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        bagage.IdBagage = sdr.GetInt32(sdr.GetOrdinal("id"));
                        bagage.CodeIata = sdr.GetString(sdr.GetOrdinal("codeIata"));
                        bagage.DateCreation = sdr.GetDateTime(sdr.GetOrdinal("dateCreation"));
                        bagage.DateModification = sdr["dateModification"] is DBNull ? DateTime.MaxValue : sdr.GetDateTime(sdr.GetOrdinal("dateModification"));
                        bagage.Vol = new Vol
                        {
                            IdVol = sdr.GetInt32(sdr.GetOrdinal("idVol")),
                            Compagnie = sdr.GetString(sdr.GetOrdinal("Compagnie")),
                            Ligne = sdr.GetString(sdr.GetOrdinal("Ligne")),
                            JourExploitation = sdr.GetInt16(sdr.GetOrdinal("JourExploitation")),
                            TypeAvion = sdr["TypeAvion"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("TypeAvion")),
                            Immatriculation = sdr["Immatriculation"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("Immatriculation")),
                            Parking = sdr["Parking"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("Parking")),
                            DernierHoraire = sdr["DernierHoraire"] is DBNull ? DateTime.MaxValue : sdr.GetDateTime(sdr.GetOrdinal("DernierHoraire")),
                            OrigineCreation = sdr["OrigineCreation"] is DBNull ? '_' : sdr.GetString(sdr.GetOrdinal("OrigineCreation")).ToCharArray()[0]
                        };

                        switch (sdr.GetString(sdr.GetOrdinal("Statut")))
                        {
                            case "ANN":
                                bagage.Vol.Statut = StatutVol.Annulé;
                                break;
                            case "PAR":
                                bagage.Vol.Statut = StatutVol.Parti;
                                break;
                            case "NORM":
                                bagage.Vol.Statut = StatutVol.Normal;
                                break;
                            case "SUP":
                                bagage.Vol.Statut = StatutVol.Supprimé;
                                break;
                            default:
                                bagage.Vol.Statut = StatutVol.Normal;
                                break;
                        }
                    }

                    sdr.NextResult();

                    while (sdr.Read())
                    {
                        var trace = new Trace
                        {
                            IdTrace = sdr.GetInt32(sdr.GetOrdinal("idTrace")),
                            IdBagage = sdr.GetInt32(sdr.GetOrdinal("idBagage")),
                            Horodate = sdr.GetDateTime(sdr.GetOrdinal("horodate")),
                            Type = sdr.GetInt32(sdr.GetOrdinal("type")),
                            Localisation = sdr.GetInt32(sdr.GetOrdinal("localisation")),
                            Information = sdr["information"] is DBNull ? " " : sdr.GetString(sdr.GetOrdinal("information")),
                            Statut = sdr["statut"] is DBNull ? 0 : sdr.GetInt32(sdr.GetOrdinal("statut"))
                        };
                        traces.Add(trace);
                    }
                    bagage.Traces = traces;
                }
            }
            return bagage;
        }

        public override List<Bagage> GetBagagesByCodeIata(string codeIata, DateTime debut, DateTime fin)
        {
            var bagages = new List<Bagage>();

            using (var cnx = new SqlConnection(ConnectionString))
            {
                var cmd = new SqlCommand("selectBagageByIataCode", cnx)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.Add("@IataCode", SqlDbType.VarChar);
                cmd.Parameters.Add("@dateDebut", SqlDbType.DateTime);
                cmd.Parameters.Add("@dateFin", SqlDbType.DateTime);
                cmd.Parameters[0].Value = codeIata;
                cmd.Parameters[1].Value = debut;
                cmd.Parameters[2].Value = fin;
                cnx.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        var bagage = new Bagage
                        {
                            IdBagage = sdr.GetInt32(sdr.GetOrdinal("id")),
                            CodeIata = sdr.GetString(sdr.GetOrdinal("codeIata")),
                            DateCreation = sdr.GetDateTime(sdr.GetOrdinal("dateCreation")),
                            DateModification = sdr["dateModification"] is DBNull ? DateTime.MaxValue : sdr.GetDateTime(sdr.GetOrdinal("dateModification")),
                            Vol = new Vol
                            {
                                IdVol = sdr.GetInt32(sdr.GetOrdinal("idVol")),
                                Compagnie = sdr.GetString(sdr.GetOrdinal("Compagnie")),
                                Ligne = sdr.GetString(sdr.GetOrdinal("Ligne")),
                                JourExploitation = sdr.GetInt16(sdr.GetOrdinal("JourExploitation")),
                                TypeAvion = sdr["TypeAvion"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("TypeAvion")),
                                Immatriculation = sdr["Immatriculation"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("Immatriculation")),
                                Parking = sdr["Parking"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("Parking")),
                                DernierHoraire = sdr["DernierHoraire"] is DBNull ? DateTime.MaxValue : sdr.GetDateTime(sdr.GetOrdinal("DernierHoraire")),
                                OrigineCreation = sdr["OrigineCreation"] is DBNull ? '_' : sdr.GetString(sdr.GetOrdinal("OrigineCreation")).ToCharArray()[0]
                            }
                        };

                        switch (sdr.GetString(sdr.GetOrdinal("Statut")))
                        {
                            case "ANN":
                                bagage.Vol.Statut = StatutVol.Annulé;
                                break;
                            case "PAR":
                                bagage.Vol.Statut = StatutVol.Parti;
                                break;
                            case "NORM":
                                bagage.Vol.Statut = StatutVol.Normal;
                                break;
                            case "SUP":
                                bagage.Vol.Statut = StatutVol.Supprimé;
                                break;
                            default:
                                bagage.Vol.Statut = StatutVol.Normal;
                                break;
                        }
                        bagages.Add(bagage);
                    }
                }
            }
            return bagages;
        }

        public override List<Bagage> GetBagages(string cie, string line, DateTime debut, DateTime fin)
        {
            var bagages = new List<Bagage>();

            using (var cnx = new SqlConnection(ConnectionString))
            {
                var cmd = new SqlCommand("selectBagagesDatesCieLine", cnx)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.Add("@compagnie", SqlDbType.VarChar);
                cmd.Parameters.Add("@line", SqlDbType.VarChar);
                cmd.Parameters.Add("@dateDebut", SqlDbType.DateTime);
                cmd.Parameters.Add("@dateFin", SqlDbType.DateTime);
                cmd.Parameters[0].Value = cie;
                cmd.Parameters[1].Value = line;
                cmd.Parameters[2].Value = debut;
                cmd.Parameters[3].Value = fin;
                cnx.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        var bagage = new Bagage
                        {
                            IdBagage = sdr.GetInt32(sdr.GetOrdinal("id")),
                            CodeIata = sdr.GetString(sdr.GetOrdinal("codeIata")),
                            DateCreation = sdr.GetDateTime(sdr.GetOrdinal("dateCreation")),
                            DateModification = sdr["dateModification"] is DBNull ? DateTime.MaxValue : sdr.GetDateTime(sdr.GetOrdinal("dateModification")),
                            Vol = new Vol
                            {
                                IdVol = sdr.GetInt32(sdr.GetOrdinal("idVol")),
                                Compagnie = sdr.GetString(sdr.GetOrdinal("Compagnie")),
                                Ligne = sdr.GetString(sdr.GetOrdinal("Ligne")),
                                JourExploitation = sdr.GetInt16(sdr.GetOrdinal("JourExploitation")),
                                TypeAvion = sdr["TypeAvion"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("TypeAvion")),
                                Immatriculation = sdr["Immatriculation"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("Immatriculation")),
                                Parking = sdr["Parking"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("Parking")),
                                DernierHoraire = sdr["DernierHoraire"] is DBNull ? DateTime.MaxValue : sdr.GetDateTime(sdr.GetOrdinal("DernierHoraire")),
                                OrigineCreation = sdr["OrigineCreation"] is DBNull ? '_' : sdr.GetString(sdr.GetOrdinal("OrigineCreation")).ToCharArray()[0]
                            }
                        };

                        switch (sdr.GetString(sdr.GetOrdinal("Statut")))
                        {
                            case "ANN":
                                bagage.Vol.Statut = StatutVol.Annulé;
                                break;
                            case "PAR":
                                bagage.Vol.Statut = StatutVol.Parti;
                                break;
                            case "NORM":
                                bagage.Vol.Statut = StatutVol.Normal;
                                break;
                            case "SUP":
                                bagage.Vol.Statut = StatutVol.Supprimé;
                                break;
                            default:
                                bagage.Vol.Statut = StatutVol.Normal;
                                break;
                        }
                        bagages.Add(bagage);
                    }
                }
            }
            return bagages;
        }

        public override List<Bagage> GetBagagesByIdVol(int idVol)
        {
            var bagages = new List<Bagage>();

            using (var cnx = new SqlConnection(ConnectionString))
            {
                var cmd = new SqlCommand("selectBagagesByVolId", cnx)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.Add("@idVol", SqlDbType.Int);
                cmd.Parameters[0].Value = idVol;
                cnx.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        var bagage = new Bagage
                        {
                            IdBagage = sdr.GetInt32(sdr.GetOrdinal("id")),
                            CodeIata = sdr.GetString(sdr.GetOrdinal("codeIata")),
                            DateCreation = sdr.GetDateTime(sdr.GetOrdinal("dateCreation")),
                            DateModification = sdr["dateModification"] is DBNull ? DateTime.MaxValue : sdr.GetDateTime(sdr.GetOrdinal("dateModification")),
                            Vol = new Vol
                            {
                                IdVol = sdr.GetInt32(sdr.GetOrdinal("idVol")),
                                Compagnie = sdr.GetString(sdr.GetOrdinal("Compagnie")),
                                Ligne = sdr.GetString(sdr.GetOrdinal("Ligne")),
                                JourExploitation = sdr.GetInt16(sdr.GetOrdinal("JourExploitation")),
                                TypeAvion = sdr["TypeAvion"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("TypeAvion")),
                                Immatriculation = sdr["Immatriculation"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("Immatriculation")),
                                Parking = sdr["Parking"] is DBNull ? String.Empty : sdr.GetString(sdr.GetOrdinal("Parking")),
                                DernierHoraire = sdr["DernierHoraire"] is DBNull ? DateTime.MaxValue : sdr.GetDateTime(sdr.GetOrdinal("DernierHoraire")),
                                OrigineCreation = sdr["OrigineCreation"] is DBNull ? '_' : sdr.GetString(sdr.GetOrdinal("OrigineCreation")).ToCharArray()[0]
                            }
                        };

                        switch (sdr.GetString(sdr.GetOrdinal("Statut")))
                        {
                            case "ANN":
                                bagage.Vol.Statut = StatutVol.Annulé;
                                break;
                            case "PAR":
                                bagage.Vol.Statut = StatutVol.Parti;
                                break;
                            case "NORM":
                                bagage.Vol.Statut = StatutVol.Normal;
                                break;
                            case "SUP":
                                bagage.Vol.Statut = StatutVol.Supprimé;
                                break;
                            default:
                                bagage.Vol.Statut = StatutVol.Normal;
                                break;
                        }
                        bagages.Add(bagage);
                    }
                }
            }
            return bagages;
        }

        public override List<Trace> GetBagageTrace(int idBagage)
        {
            var traces = new List<Trace>();

            using (var cnx = new SqlConnection(ConnectionString))
            {
                var cmd = new SqlCommand("selectTraces", cnx)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.Add("@idBagage", SqlDbType.Int);
                cmd.Parameters[0].Value = idBagage;
                cnx.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        var trace = new Trace
                        {
                            IdTrace = sdr.GetInt32(sdr.GetOrdinal("idTrace")),
                            IdBagage = sdr.GetInt32(sdr.GetOrdinal("idBagage")),
                            Horodate = sdr.GetDateTime(sdr.GetOrdinal("horodate")),
                            Type = sdr.GetInt32(sdr.GetOrdinal("type")),
                            Localisation = sdr.GetInt32(sdr.GetOrdinal("localisation")),
                            Information = sdr["information"] is DBNull ? " " : sdr.GetString(sdr.GetOrdinal("information")),
                            Statut = sdr["statut"] is DBNull ? 0 : sdr.GetInt32(sdr.GetOrdinal("statut"))
                        };
                        traces.Add(trace);
                    }
                }
            }
            return traces;
        }
    }
}
