﻿using Airport.Service;
using System;
using System.ServiceModel;
using System.Windows.Forms;

namespace Airport.HostService
{
    public partial class Form1 : Form
    {
        private ServiceHost _host;

        public Form1()
        {
            InitializeComponent();
            InitHost();

            this.Text = "Airport Service";
            label4.Text = AirportService.InstanceEnCours.ToString();
            label6.Text = AirportService.InstanceCreees.ToString();
        }

        private void InitHost()
        {
            _host = new ServiceHost(typeof(AirportService));
            _host.Closed += host_closed;
            _host.Closing += host_closing;
            _host.Faulted += host_faulted;
            _host.Opening += host_opening;
            _host.Opened += host_opened;
            // Unknown messages
            _host.UnknownMessageReceived += host_unknownMessageReceived;

            label2.Text = _host.State.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            switch (_host.State)
            {
                case CommunicationState.Created:
                    _host.Open();
                    button1.Text = "Close";
                    break;
                case CommunicationState.Opened:
                    _host.Close();
                    button1.Text = "Launch";
                    InitHost();
                    break;
                default:
                    _host.Close();
                    button1.Text = "Launch";
                    InitHost();
                    break;

            }
        }

        private void host_unknownMessageReceived(Object sender, UnknownMessageReceivedEventArgs e)
        {
            string erreur = e.Message.Headers.Action;
        }

        private void host_closed(Object sender, EventArgs e)
        {
            label2.Text = _host.State.ToString();
            listBox1.Items.Add("Changement d'état du host : " + _host.State.ToString());
        }

        private void host_closing(Object sender, EventArgs e)
        {
            label2.Text = _host.State.ToString();
            listBox1.Items.Add("Changement d'état du host : " + _host.State.ToString());
        }

        private void host_faulted(Object sender, EventArgs e)
        {
            label2.Text = _host.State.ToString();
            listBox1.Items.Add("Changement d'état du host : " + _host.State.ToString());
        }

        private void host_opening(Object sender, EventArgs e)
        {
            label2.Text = _host.State.ToString();
            listBox1.Items.Add("Changement d'état du host : " + _host.State.ToString());
        }

        private void host_opened(Object sender, EventArgs e)
        {
            label2.Text = _host.State.ToString();
            listBox1.Items.Add("Changement d'état du host : " + _host.State.ToString());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label4.Text = AirportService.InstanceEnCours.ToString();
            label6.Text = AirportService.InstanceCreees.ToString();
        }
    }
}
