USE [CDG1_EXPLOIT_ADP]
GO

/****** Object:  StoredProcedure [dbo].[selectBagagesDatesCieLine]    Script Date: 11/01/2014 19:53:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		C�sar Berezowski
-- Create date: 09/01/2014
-- Description:	Get bagages from specified vol
-- =============================================
CREATE PROCEDURE [dbo].[selectBagagesDatesCieLine] 
	-- Add the parameters for the stored procedure here
	@compagnie VarChar(3), 
	@line VarChar(5),
	@dateDebut DateTime,
	@dateFin DateTime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT b.ID_BSM as id, b.CODE_IATA AS codeIata, v.ID_VOL as idVol , b.DAT_CRE AS dateCreation, b.DAT_MOD as dateModification, v.ID_VOL, c.CODE_CIE AS Compagnie, v.LIG AS Ligne, v.JEX AS JourExploitation, v.STA AS Statut, vd.TYP AS TypeAvion, vd.IMM AS Immatriculation, v.PKG AS Parking, v.DHC AS DernierHoraire, vd.CRE AS OrigineCreation 
	FROM BSM b INNER JOIN VOL v ON b.ID_VOL = v.ID_VOL INNER JOIN CIE c ON c.ID_CIE = v.ID_CIE INNER JOIN VOL_DEPART vd ON vd.ID_VOL = v.ID_VOL 
	WHERE b.CIEE=@compagnie AND b.LIGE=@line AND b.DAT_CRE BETWEEN @dateDebut AND @dateFin 
END

GO


