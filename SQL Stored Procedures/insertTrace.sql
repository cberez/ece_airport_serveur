USE [CDG1_EXPLOIT_ADP]
GO

/****** Object:  StoredProcedure [dbo].[insertTrace]    Script Date: 11/01/2014 19:51:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		C�sar Berezowski
-- Create date: 11/01/2014
-- Description:	Insert a new trace linked to an occurence and a bagage
-- =============================================
CREATE PROCEDURE [dbo].[insertTrace] 
	-- Add the parameters for the stored procedure here
	@idBsm int = 0, 
	@horodate dateTime = NULL,
	@type int = 0,
	@localisation int = 0,
	@info int = 0,
	@statut int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @ID_BAGAGE INT = 0;
	SET @ID_BAGAGE = (SELECT MAX(ID_BAGAGE) FROM OCCURENCE_BAGAGE) + 1;

	DECLARE @ORDRE_APPARITION INT = 0;
	SET @ORDRE_APPARITION = (SELECT MAX(ORDRE_APPARITION) FROM OCCURENCE_BAGAGE WHERE ID_BSM = @idBsm) + 1;

	IF(@ORDRE_APPARITION IS NULL)
		SET @ORDRE_APPARITION = 0;

	INSERT INTO OCCURENCE_BAGAGE(ID_BAGAGE,  ID_BSM, ORDRE_APPARITION)
	VALUES						(@ID_BAGAGE, @idBsm, @ORDRE_APPARITION);

	DECLARE @ID_TRACE_BAG INT = 0;
	SET @ID_TRACE_BAG = (SELECT MAX(ID_TRACE_BAG) FROM TRACE_BAGAGE) + 1;

	INSERT INTO TRACE_BAGAGE(ID_TRACE_BAG,  ID_BAGAGE, TTRACE,	   ID_LOC,		  TYPE,  STATUT,  INFO)
	VALUES					(@ID_TRACE_BAG, @ID_BAGAGE, @horodate, @localisation, @type, @statut, @info);

END

GO


