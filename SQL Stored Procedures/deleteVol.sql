USE [CDG1_EXPLOIT_ADP]
GO

/****** Object:  StoredProcedure [dbo].[deleteVol]    Script Date: 11/01/2014 19:51:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		C�sar Berezowski
-- Create date: 10/01/2014
-- Description:	Delete a vol
-- =============================================
CREATE PROCEDURE [dbo].[deleteVol] 
	-- Add the parameters for the stored procedure here
	@idVol int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM  VOL_DEPART
		WHERE ID_VOL = @idVol;

	DELETE
		FROM  VOL
		WHERE ID_VOL = @idVol;
END

GO


