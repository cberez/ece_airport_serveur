USE [CDG1_EXPLOIT_ADP]
GO

/****** Object:  StoredProcedure [dbo].[insertVol]    Script Date: 11/01/2014 19:51:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		C�sar Berezowski
-- Create date: 15/11/2013
-- Description:	Create a new Vol
-- =============================================
CREATE PROCEDURE [dbo].[insertVol] 
	-- Add the parameters for the stored procedure here
	@compagnie char(3) = '', 
	@ligne varchar(5) = '', 
	@joursExploit int = 0, 
	@dernierHoraire datetime = null, 
	@statut varchar(4) = '', 
	@parking char(3) = '',
	@typeAvion char(3) = '',
	@immatriculation char(5) = '',
	@origineCreation char(1) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @ID_VOL INT = 0;
	SET  @ID_VOL = (SELECT MAX(ID_VOL) FROM VOL) + 1;

	DECLARE @ID_CIE INT = 0;
	SET @ID_CIE = (SELECT ID_CIE AS idCompagnie FROM CIE WHERE CODE_CIE = @compagnie);
	IF(@ID_CIE IS NULL)  
		SET @ID_CIE = '-1';
		

	INSERT INTO VOL (ID_VOL,  ID_CIE,  LIG,    JEX,           DHC,             STA,     PKG,		QRF,  EST_VOL_DEP)
	VALUES          (@ID_VOL, @ID_CIE, @ligne, @joursExploit, @dernierHoraire, @statut, @parking, '-1', '0');

	INSERT INTO VOL_DEPART(ID_VOL, TYP,		   IMM,				 CRE,			   SHA)
	VALUES				  (@ID_VOL, @typeAvion, @immatriculation, @origineCreation, '_'); 
END

GO


