USE [CDG1_EXPLOIT_ADP]
GO

/****** Object:  StoredProcedure [dbo].[selectDetailsVol]    Script Date: 11/01/2014 19:53:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		C�sar Berezowski
-- Create date: 08/11/2013
-- Description:	Get details from vol with specified id
-- =============================================
CREATE PROCEDURE [dbo].[selectDetailsVol] 
	-- Add the parameters for the stored procedure here
	@idVol int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT v.ID_VOL, c.CODE_CIE AS Compagnie, v.LIG AS Ligne, v.JEX AS JourExploitation, v.STA AS Statut, vd.TYP AS TypeAvion, vd.IMM AS Immatriculation, v.PKG AS Parking, v.DHC AS DernierHoraire, vd.CRE AS OrigineCreation 
	FROM VOL v INNER JOIN VOL_DEPART vd ON vd.ID_VOL = v.ID_VOL INNER JOIN CIE c ON c.ID_CIE = v.ID_CIE 
	WHERE v.ID_VOL=@idVol;

	SELECT CODE_VILLE AS Ville 
	FROM VOL_ITI 
	WHERE ID_VOL=@idVol 
	ORDER BY ORDRE_ESCALE;

	SELECT b.ID_SIE as Banque 
	FROM VOL_ENRGT_SUR ve INNER JOIN RES_BNQ b ON ve.ID_RES = b.ID_RES 
	WHERE ve.ID_VOL=@idVol 
	ORDER BY 1;
END

GO


