USE [CDG1_EXPLOIT_ADP]
GO

/****** Object:  StoredProcedure [dbo].[insertBagage]    Script Date: 11/01/2014 19:51:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		C�sar Berezowski
-- Create date: 10/01/2014
-- Description:	Insert a new Bagage
-- =============================================
CREATE PROCEDURE [dbo].[insertBagage] 
	-- Add the parameters for the stored procedure here
	@codeIata char(12) = '',
	@dateCreation datetime = NULL,
	@idVol int = NULL,
	@cie varchar(3) = '_',
	@ligne varchar(5) = '_'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @ID_BSM INT = 0;
	IF(@ID_BSM = 0)
		SET @ID_BSM = (SELECT MAX(ID_BSM) FROM BSM) + 1;
	
	IF(@idVol IS NOT NULL)
		BEGIN
			IF(@cie = '_')
				SET @cie = (SELECT c.CODE_CIE FROM VOL v INNER JOIN CIE c ON c.ID_CIE = v.ID_CIE WHERE v.ID_VOL = @idVol);
			
			IF(@ligne = '_')
				SET @ligne = (SELECT LIG FROM VOL WHERE ID_VOL = @idVol);
		END

    -- Insert statements for procedure here
	INSERT INTO BSM (ID_BSM,  ID_VOL, CODE_IATA, CRE, DAT_CRE,		 CIEE, LIGE,   JEXE, EXT, MOD, SUP, REINTRO, REAPPARU, CHGVOL, EMB, STATUT_EJECTION, STATUT_TEMPOREL)
	VALUES			(@ID_BSM, @idVol, @codeIata, 'A', @dateCreation, @cie, @ligne, '-1', '0', '0', '0', '0',	 '0',	   '0',	   '0', '0',			 '0');
END

GO


