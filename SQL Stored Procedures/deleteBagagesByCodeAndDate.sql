USE [CDG1_EXPLOIT_ADP]
GO

/****** Object:  StoredProcedure [dbo].[deleteBagageByCodeAndDate]    Script Date: 11/01/2014 19:50:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		C�sar Berezowski
-- Create date: 10/01/2014
-- Description:	Delete a bagage using its iata code and creation date
-- =============================================
CREATE PROCEDURE [dbo].[deleteBagageByCodeAndDate] 
	-- Add the parameters for the stored procedure here
	@iataCode char(12) = '', 
	@dateCrea dateTime = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @ID INT = 0;
	SET @ID = (SELECT ID_BSM FROM BSM WHERE CODE_IATA = @iataCode AND DAT_CRE = @dateCrea);

	IF(@ID IS NOT NULL)
		DELETE 
		FROM OCCURENCE_BAGAGE
		WHERE ID_BSM = @ID;

		DELETE
		FROM BSM
		WHERE ID_BSM = @ID;
END

GO


