USE [CDG1_EXPLOIT_ADP]
GO

/****** Object:  StoredProcedure [dbo].[selectBagagesByVolId]    Script Date: 11/01/2014 19:53:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		C�sar Berezowski
-- Create date: 09/01/2014
-- Description:	Select bagages from specified vol
-- =============================================
CREATE PROCEDURE [dbo].[selectBagagesByVolId] 
	-- Add the parameters for the stored procedure here
	@idVol int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT b.ID_BSM as id, b.CODE_IATA AS codeIata, b.ID_VOL as idVol , b.DAT_CRE AS dateCreation, b.DAT_MOD as dateModification, c.CODE_CIE AS Compagnie, v.LIG AS Ligne, v.JEX AS JourExploitation, v.STA AS Statut, vd.TYP AS TypeAvion, vd.IMM AS Immatriculation, v.PKG AS Parking, v.DHC AS DernierHoraire, vd.CRE AS OrigineCreation 
	FROM BSM b INNER JOIN VOL v ON b.ID_VOL = v.ID_VOL INNER JOIN CIE c ON c.ID_CIE = v.ID_CIE INNER JOIN VOL_DEPART vd ON vd.ID_VOL = v.ID_VOL 
	WHERE b.ID_VOL = @idVol;
END


GO


