USE [CDG1_EXPLOIT_ADP]
GO

/****** Object:  StoredProcedure [dbo].[updateVolBagage]    Script Date: 11/01/2014 19:54:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		C�sar Berezowski
-- Create date: 10/01/2014
-- Description:	Update a bagage's vol id
-- =============================================
CREATE PROCEDURE [dbo].[updateVolBagage] 
	-- Add the parameters for the stored procedure here
	@id int = 0, 
	@idVol int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE BSM
	SET ID_VOL = @idVol
	WHERE ID_BSM = @id;
END

GO


