USE [CDG1_EXPLOIT_ADP]
GO

/****** Object:  StoredProcedure [dbo].[selectAllVolsDatesCieLine]    Script Date: 11/01/2014 19:52:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		C�sar Berezowski
-- Create date: 08/11/2013
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[selectAllVolsDatesCieLine] 
	-- Add the parameters for the stored procedure here
	@compagnie VarChar(3), 
	@line VarChar(5),
	@dateDebut DateTime,
	@dateFin DateTime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT v.ID_VOL, c.CODE_CIE AS Compagnie, v.LIG AS Ligne, v.JEX AS JourExploitation, v.STA AS Statut, vd.TYP AS TypeAvion, vd.IMM AS Immatriculation, v.PKG AS Parking, v.DHC AS DernierHoraire, vd.CRE AS OrigineCreation 
	FROM VOL v INNER JOIN VOL_DEPART vd ON vd.ID_VOL = v.ID_VOL INNER JOIN CIE c ON c.ID_CIE = v.ID_CIE 
	WHERE c.CODE_CIE=@compagnie AND v.LIG=@line AND v.DHC BETWEEN @dateDebut AND @dateFin
END

GO


