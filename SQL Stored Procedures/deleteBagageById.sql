USE [CDG1_EXPLOIT_ADP]
GO

/****** Object:  StoredProcedure [dbo].[deleteBagageById]    Script Date: 11/01/2014 19:51:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		C�sar Berezowski
-- Create date: 10/10/2014
-- Description:	Delete a bagage by its id or vol id
-- =============================================
CREATE PROCEDURE [dbo].[deleteBagageById] 
	-- Add the parameters for the stored procedure here
	@id int = NULL, 
	@idVol int = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF (@id IS NULL)
		BEGIN
			DELETE o FROM OCCURENCE_BAGAGE o INNER JOIN BSM b ON o.ID_BSM = b.ID_BSM  WHERE ID_VOL = @idVol;
			DELETE FROM BSM WHERE ID_VOL = @idVol;
		END
	ELSE
		BEGIN
			DELETE FROM OCCURENCE_BAGAGE WHERE ID_BSM = @id;
			DELETE FROM BSM WHERE ID_BSM = @id;
		END
END
GO


