USE [CDG1_EXPLOIT_ADP]
GO

/****** Object:  StoredProcedure [dbo].[selectTraces]    Script Date: 11/01/2014 19:53:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		C�sar Berezowski
-- Create date: 10/01/2014
-- Description:	Select all traces related to a bagage
-- =============================================
CREATE PROCEDURE [dbo].[selectTraces] 
	-- Add the parameters for the stored procedure here
	@idBagage int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT t.ID_TRACE_BAG as idTrace, t.ID_BAGAGE as IdBagage, t.TTRACE as horodate, t.TYPE as type, t.ID_LOC as localisation, t.INFO as information, t.STATUT as statut 
	FROM TRACE_BAGAGE t INNER JOIN OCCURENCE_BAGAGE o ON t.ID_BAGAGE = o.ID_BAGAGE INNER JOIN BSM b ON o.ID_BSM = b.ID_BSM
	WHERE b.ID_BSM = @idBagage
	ORDER BY o.ORDRE_APPARITION;
END

GO


