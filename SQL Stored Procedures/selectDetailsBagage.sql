USE [CDG1_EXPLOIT_ADP]
GO

/****** Object:  StoredProcedure [dbo].[selectDetailsBagage]    Script Date: 11/01/2014 19:53:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		C�sar Berezowski
-- Create date: 11/01/2014
-- Description:	Select details of one bagage
-- =============================================
CREATE PROCEDURE [dbo].[selectDetailsBagage] 
	-- Add the parameters for the stored procedure here
	@idBagage int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT b.ID_BSM as id, b.CODE_IATA AS codeIata, b.ID_VOL as idVol , b.DAT_CRE AS dateCreation, b.DAT_MOD as dateModification, c.CODE_CIE AS Compagnie, v.LIG AS Ligne, v.JEX AS JourExploitation, v.STA AS Statut, vd.TYP AS TypeAvion, vd.IMM AS Immatriculation, v.PKG AS Parking, v.DHC AS DernierHoraire, vd.CRE AS OrigineCreation 
	FROM BSM b INNER JOIN VOL v ON b.ID_VOL = v.ID_VOL INNER JOIN CIE c ON c.ID_CIE = v.ID_CIE INNER JOIN VOL_DEPART vd ON vd.ID_VOL = v.ID_VOL 
	WHERE b.ID_BSM = @idBagage;

	SELECT t.ID_TRACE_BAG as idTrace, t.ID_BAGAGE as IdBagage, t.TTRACE as horodate, t.TYPE as type, t.ID_LOC as localisation, t.INFO as information, t.STATUT as statut 
	FROM TRACE_BAGAGE t INNER JOIN OCCURENCE_BAGAGE o ON t.ID_BAGAGE = o.ID_BAGAGE INNER JOIN BSM b ON o.ID_BSM = b.ID_BSM
	WHERE b.ID_BSM = @idBagage
	ORDER BY o.ORDRE_APPARITION;

END

GO


