﻿using Airport.Entities;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace Airport.Service
{
    [ServiceContract]
    public interface IAirportService
    {
        [FaultContract(typeof(DataFaultException))]

        #region Vols
        [OperationContract]
        List<Vol> GetAllVols();

        [OperationContract]
        void CreateVol(string cie, string line, int jourEx, string statut, string immatriculation, string pkg, DateTime dernierHoraire, char origineCreation, string typeAvion);

        [OperationContract]
        void DeleteVol(int id);

        [OperationContract]
        List<Vol> GetVolsByDates(DateTime debut, DateTime fin);
        
        [OperationContract]
        List<Vol> GetVolsByCieAndLineAndDates(string cie, string line, DateTime debut, DateTime fin);

        [OperationContract]
        Vol GetDetailsVol(int idVol);
        #endregion

        #region Bagages
        [OperationContract]
        void CreateBagage(string codeIata, DateTime dateCreation, int idVol, string cie, string ligne);

        [OperationContract]
        void UpdateVolBagage(int idBagage, int idVol);

        [OperationContract]
        void DeleteBagageByCodeAndDate(string codeIata, DateTime dateCreation);

        [OperationContract]
        void DeleteBagageById(int id);

        [OperationContract]
        void DeleteBagageByIdVol(int idVol);

        [OperationContract]
        void AddTraceBagage(int idBagage, DateTime horodate, int type, int localisation, string information, int statut);

        [OperationContract]
        Bagage GetDetailBagage(int idBagage);

        [OperationContract]
        List<Bagage> GetBagagesByIdVol(int idVol);

        [OperationContract]
        List<Bagage> GetBagagesByCodeIata(string codeIata, DateTime debut, DateTime fin);

        [OperationContract]
        List<Bagage> GetBagages(string cie, string line, DateTime debut, DateTime fin);

        [OperationContract]
        List<Trace> GetBagageTrace(int idBagage);
        #endregion
    }
}
