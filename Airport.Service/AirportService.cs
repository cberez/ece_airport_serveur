﻿using Airport.Entities;
using Airport.Models;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace Airport.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class AirportService : IAirportService
    {
        private readonly AbstractModel _model = ModelsFactory.Model;
        public static int InstanceEnCours = 0;
        public static int InstanceCreees = 0;

        public AirportService()
        {
            InstanceEnCours++;
            InstanceCreees++;
        }

        ~AirportService()
        {
            InstanceEnCours--;
        }

        public void CreateVol(string cie, string line, int jourEx, string statut, string immatriculation, string pkg, DateTime dernierHoraire, char origineCreation, string typeAvion)
        {
            try
            {
                _model.CreateVol(cie, line, jourEx, statut, immatriculation, pkg, dernierHoraire, origineCreation, typeAvion);
            }
            catch
            {
                throw new FaultException("Erreur dans le traitement de la requête");
            }
        }

        public void DeleteVol(int id)
        {
            try
            {
                _model.DeleteVol(id);
            }
            catch
            {
                throw new FaultException("Erreur dans le traitement de la requête");
            }
        }

        public List<Vol> GetAllVols()
        {
            try
            {
                return _model.GetVols();
            }
            catch
            {
                //DataFaultException e = new DataFaultException() {Message = "Erreur dans le traitement de la requête"};
                //throw new FaultException<DataFaultException>(e);
                return null;
            }
        }

        public List<Vol> GetVolsByDates(DateTime debut, DateTime fin)
        {
            try
            {
                return _model.GetVols(debut, fin);
            }
            catch
            {
                throw new FaultException("Erreur dans le traitement de la requête");
            }
        }

        public List<Vol> GetVolsByCieAndLineAndDates(string cie, string line, DateTime debut, DateTime fin)
        {
            try
            {
                return _model.GetVols(cie, line, debut, fin);
            }
            catch
            {
                throw new FaultException("Erreur dans le traitement de la requête");
            }
        }

        public Vol GetDetailsVol(int idVol)
        {
            try
            {
                return _model.GetDetailsVol(idVol);
            }
            catch
            {
                throw new FaultException("Erreur dans le traitement de la requête");
            }
        }

        public void CreateBagage(string codeIata, DateTime dateCreation, int idVol, string cie, string ligne)
        {
            try
            {
                _model.CreateBagage(codeIata, dateCreation, idVol, cie, ligne);
            }
            catch
            {
                throw new FaultException("Erreur dans le traitement de la requête");
            }
        }

        public void UpdateVolBagage(int idBagage, int idVol)
        {
            try
            {
                _model.UpdateVolBagage(idBagage, idVol);
            }
            catch
            {
                throw new FaultException("Erreur dans le traitement de la requête");
            }
        }

        public void DeleteBagageByCodeAndDate(string codeIata, DateTime dateCreation)
        {
            try
            {
                _model.DeleteBagageByCodeAndDate(codeIata, dateCreation);
            }
            catch
            {
                throw new FaultException("Erreur dans le traitement de la requête");
            }
        }

        public void DeleteBagageById(int id)
        {
            try
            {
                _model.DeleteBagageById(id);
            }
            catch
            {
                throw new FaultException("Erreur dans le traitement de la requête");
            }
        }

        public void DeleteBagageByIdVol(int idVol)
        {
            try
            {
                _model.DeleteBagageByIdVol(idVol);
            }
            catch
            {
                throw new FaultException("Erreur dans le traitement de la requête");
            }
        }

        public void AddTraceBagage(int idBagage, DateTime horodate, int type, int localisation, string information, int statut)
        {
            try
            {
                _model.AddTraceBagage(idBagage, horodate, type, localisation, information, statut);
            }
            catch
            {
                throw new FaultException("Erreur dans le traitement de la requête");
            }
        }

        public Bagage GetDetailBagage(int idBagage)
        {
            try
            {
                return _model.GetDetailBagage(idBagage);
            }
            catch
            {
                throw new FaultException("Erreur dans le traitement de la requête");
            }
        }

        public List<Bagage> GetBagagesByIdVol(int idVol)
        {
            try
            {
                return _model.GetBagagesByIdVol(idVol);
            }
            catch
            {
                throw new FaultException("Erreur dans le traitement de la requête");
            }
        }

        public List<Bagage> GetBagagesByCodeIata(string codeIata, DateTime debut, DateTime fin)
        {
            try
            {
                return _model.GetBagagesByCodeIata(codeIata, debut, fin);
            }
            catch
            {
                throw new FaultException("Erreur dans le traitement de la requête");
            }
        }

        public List<Bagage> GetBagages(string cie, string line, DateTime debut, DateTime fin)
        {
            try
            {
                return _model.GetBagages(cie, line, debut, fin);
            }
            catch
            {
                throw new FaultException("Erreur dans le traitement de la requête");
            }
        }

        public List<Trace> GetBagageTrace(int idBagage)
        {
            try
            {
                return _model.GetBagageTrace(idBagage);
            }
            catch
            {
                throw new FaultException("Erreur dans le traitement de la requête");
            }
        }
    }
}
